/**
 * \file Serveur.cpp
 * \brief Gestion du serveur
 * \author J.Cassagne
 */

#include "Serveur.h"

Serveur::Serveur(std::vector<Joueur> *tbl, Joueur *j, Mapping *m)
{
    arret = false;
    connecter = false;
    tblJoueurs = tbl;
    joueur = j;
    map = m;
    serveur.setBlocking(true);
    thread1 = new sf::Thread(&Serveur::TraitementPaquet, this);
    thread2 = new sf::Thread(&Serveur::EnvoiPaquet, this);
    thread1->launch();
}

Serveur::~Serveur()
{
    this->Deconnection();
    delete thread1;
    delete thread2;

    for(unsigned int i = 0 ; i < tblJoueurs->size() ; i++)
    {
        delete (*tblJoueurs)[i].anim[COURS];
        delete (*tblJoueurs)[i].anim[TOMBE];
        delete (*tblJoueurs)[i].anim[SAUTE];
    }
}

void Serveur::Deconnection()
{
    if(!connecter)
    {
        arret = true;
        thread1->terminate();
        thread2->wait();
        return;
    }
    sf::Packet paquet;
    paquet << sf::Uint16(2);
    serveur.send(paquet);
    serveur.disconnect();
    connecter = false;
    if(arret)
        return;
    arret = true;
    thread1->terminate();
    thread2->terminate();
}

bool Serveur::Connection()
{
    std::string ip;
    std::ifstream file("./data/config.txt", std::ifstream::in);
    if(!file)
        ip = "127.0.0.1";
    else
        std::getline(file, ip);
    file.close();
    serveur.setBlocking(true);
    if(serveur.connect(ip, PORT) != sf::Socket::Done)
    {
        std::cout << "Erreur 01" << std::endl;
        arret = true;
    }
    else
    {
        //thread1->launch();
        sf::Packet paquet;
        paquet << sf::Uint16(1) << joueur->nom.getString();
        serveur.send(paquet);
        arret = false;
    }
    serveur.setBlocking(false);
    return !arret;
}

void Serveur::TraitementPaquet()
{
    while(!Connection()){}

    sf::Packet paquet;
    while(!arret)
    {
        if(serveur.receive(paquet) == sf::Socket::Done)
        {
            sf::Uint16 type;
            paquet >> type;
            if(type == 0)
            {
                connecter = true;
                sf::Uint32 score;
                paquet >> score;
                //paquet >> score;
                joueur->score = score;

                thread2->launch();
            }
            else if(type == 1)
            {
                sf::Uint32 ID;
                sf::String nom;
                sf::Color couleur;
                sf::Uint32 score;
                paquet >> ID >> nom >> couleur.r >> couleur.g >> couleur.b >> score;
                Joueur nouveau;
                nouveau.nom.setString(nom);
                nouveau.ID = ID;
                nouveau.couleur = couleur;
                nouveau.score = score;
                nouveau.anim[COURS] = new Animation("./data/run.anim");
                nouveau.anim[TOMBE] = new Animation("./data/tombe.anim");
                nouveau.anim[SAUTE] = new Animation("./data/saute.anim");
                nouveau.animationActuel = COURS;
                nouveau.position = sf::Vector2f(0,-200);
                tblJoueurs->push_back(Joueur(nouveau));
                OrdonneTbl(*tblJoueurs);
            }
            else if(type == 2)
            {
                sf::Uint32 ID;
                paquet >> ID;
                for(unsigned int i = 0 ; i < tblJoueurs->size() ; i++)
                    if((*tblJoueurs)[i].ID == ID)
                    {
                        Joueur j = (*tblJoueurs)[i];
                        tblJoueurs->erase(tblJoueurs->begin()+i);
                        delete j.anim[COURS];
                        delete j.anim[TOMBE];
                        delete j.anim[SAUTE];
                        break;
                    }
            }
            else if(type == 3)
            {
                sf::Uint32 ID;
                sf::Vector2f pos;
                sf::Uint16 anim;
                paquet >> ID >> pos.x >> pos.y >> anim;

                for(unsigned int i = 0 ; i < tblJoueurs->size() ; i++)
                    if((*tblJoueurs)[i].ID == ID)
                    {
                        (*tblJoueurs)[i].position = pos;
                        if((*tblJoueurs)[i].animationActuel != anim)
                            (*tblJoueurs)[i].anim[anim]->Relancer();
                        (*tblJoueurs)[i].animationActuel = anim;
                        break;
                    }
            }
            else if(type == 4)
            {
                sf::Uint32 ID;
                int score;
                paquet >> ID >> score;
                for(unsigned int i = 0 ; i < tblJoueurs->size() ; i++)
                    if((*tblJoueurs)[i].ID == ID)
                    {
                        (*tblJoueurs)[i].score = score;
                        OrdonneTbl(*tblJoueurs);
                        break;
                    }
            }
            else if(type == 5)
            {
                std::string nom;
                paquet >> nom;
                std::ofstream file(std::string("./data/"+nom+".map").c_str(), std::ofstream::out | std::ofstream::trunc);
                while(!paquet.endOfPacket())
                {
                    std::string ligne;
                    paquet >> ligne;
                    file << ligne << "\n";
                }
                file.close();
                map->getMutex()->lock();
                map->LireFichier(nom);
                map->getMutex()->unlock();
                joueur->score = 0;
                joueur->position = map->getSpawn();
                joueur->anim[joueur->animationActuel]->getSprite()->setPosition(sf::Vector2f(0,-200));
            }
            paquet.clear();
        }
    }
}

void Serveur::EnvoiPaquet()
{
    sf::Packet paquet;
    while(!arret)
    {
        sf::sleep(sf::milliseconds(100));
        paquet.clear();
        paquet << sf::Uint16(3) << joueur->position.x << joueur->position.y << joueur->animationActuel;
        if(serveur.send(paquet) == sf::Socket::Disconnected)
        {
            thread1->terminate();
            connecter = false;
            for(unsigned int i = 0 ; i < tblJoueurs->size() ; i++)
            {
                Joueur j = (*tblJoueurs)[i];
                tblJoueurs->erase(tblJoueurs->begin()+i);
                delete j.anim[COURS];
                delete j.anim[TOMBE];
                delete j.anim[SAUTE];
            }
            thread1->launch();
            thread2->terminate();
        }
    }
}

void Serveur::EnvoiHightScore(int hs)
{
    sf::Packet paquet;
    paquet << sf::Uint16(4) << hs;
    if(this->getConnecter())
        serveur.send(paquet);
}

bool Serveur::getConnecter()
{
    return connecter;
}

unsigned int Serveur::getNbJoueurs()
{
    return tblJoueurs->size();
}

void Serveur::OrdonneTbl(std::vector<Joueur> &tbl)
{
    for(unsigned int i = 0 ; i < tbl.size() ; i++)
        for(unsigned int j = 0 ; j < tbl.size()-1 ; j++)
            if(tbl[j].score < tbl[j+1].score)
            {
                std::swap(tbl[j], tbl[j+1]);
            }

}
