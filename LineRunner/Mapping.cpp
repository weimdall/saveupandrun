/**
 * \file Mapping.cpp
 * \brief Gestion du TileMapping
 * \author J.Cassagne
 */

#include "Mapping.h"
#include <iostream>

Mapping::Mapping(sf::RenderWindow *App)
{
    //Initialisation des param�tres par d�faut
    fen = App;
    m_nbTileHauteur = 0;
    m_nbTileLargeur = 0;
    m_resolutionHauteur = 0;
    m_resolutionLargeur = 0;
    m_nbBlocsLargeur = 0;
    m_nbBlocsHauteur = 0;
}

Mapping::~Mapping()
{
   multiCalques.clear();
}

bool Mapping::LireFichier(std::string nomFichier)
{
    std::string chaine;
    int param[4];
    float paramf[2];
    int RVB[3];
    std::ifstream fichier(std::string("./data/"+nomFichier+".map").c_str(), std::ios::in);
    fichier >> chaine; //On recupere la premi�re ligne
    if(chaine != "Tilemapping-Version-1.0")
        return false;

    for(unsigned int i = 0 ; i < multiCalques.size() ; i++)
    {
        for(unsigned int j = 0 ; j < multiCalques[i].size() ; j++)
            multiCalques[i][j].clear();
        multiCalques[i].clear();
    }
    multiCalques.clear();
    m_vide.clear();






    fichier >> chaine; //On recupere le chemin vers l'image
    if(!m_ref.loadFromFile(MAPS+chaine))
        return false;

    //Extraction des param�tres de la map
    fichier >> param[0] >> param[1] >> param[2] >> param[3];
    m_nbTileLargeur = param[0];
    m_nbTileHauteur = param[1];
    m_resolutionLargeur = param[2];
    m_resolutionHauteur = param[3];

    fichier >> param[0] >> param[1];
    m_nbBlocsLargeur = param[0];
    m_nbBlocsHauteur = param[1];

    fichier >> paramf[0] >> paramf[1];
    m_scaleX = paramf[0];
    m_scaleY = paramf[1];

    //Lecture de la couleur � rendre transparent
    fichier >> RVB[0] >> RVB[1] >> RVB[2];
    m_ref.createMaskFromColor(sf::Color(RVB[0], RVB[1], RVB[2]));
    m_refText.loadFromImage(m_ref);
    m_refText.setSmooth(false);
    m_refText.setRepeated(false);

    //Lecture du lieu de spawn
    fichier >> param[0] >> param[1];
    spawn.x = m_resolutionLargeur*param[0]*m_scaleX;
    spawn.y = m_resolutionHauteur*param[1]*m_scaleY;

    //liste des blocs vide (pour les collision)
    fichier >> param[0];
    while(param[0] != -1)
    {
        m_vide.push_back(param[0]-1);
        fichier >> param[0];
    }

    //enregistrement de la matrice et des differents calques
    std::vector <Tile>tblTemp;
    std::vector <std::vector <Tile> >calque;
    do
    {
        for(int i = 0 ; i < m_nbBlocsHauteur ; i++)
        {
            for(int j = 0 ; j < m_nbBlocsLargeur ; j++)
            {
                Tile tileTemp;
                fichier >> tileTemp.bloc;
                tileTemp.bloc--;
                tileTemp.opacity=0;
                tileTemp.affiche = false;
                tblTemp.push_back(tileTemp);
            }
            calque.push_back(tblTemp);
            tblTemp.clear();
        }
        this->ChargerMap(calque);
        multiCalques.push_back(calque);
        calque.clear();
        fichier >> param[0];
    }while(param[0] == -1 && !fichier.eof());
    fichier.close();
    nomMap=nomFichier;

    return true;
}

bool Mapping::ChargerMap(std::vector <std::vector<Tile> >&calque)
{
    //Algorithme d�coupant les blocs en fonction de leurs ID (ID comprit entre 0 et (m_nbTileHauteur*m_nbTileLargeur)-1 )
    for(unsigned int i = 0 ; i < calque.size() ; i++)
    {
        for(unsigned int j = 0 ; j < calque[i].size() ; j++)
        {
            int bloc = calque[i][j].bloc; //On attribut � bloc l'ID du bloc sur lequel on va travailler (pour simplifier les caculs)
            if(bloc >= 0)
            {
                if(bloc > (m_nbTileHauteur*m_nbTileLargeur)-1) //Si l'ID n'est pas comprise dans le tileset.bmp
                    return false;

                int y =  int((bloc/m_nbTileLargeur)); //y � l'endroit (en y) ou ce trouve la case dans le tileset.bmp
                int x = (bloc-(y*m_nbTileLargeur))%m_nbTileLargeur; //idem en x

                y *= m_resolutionHauteur;
                x *= m_resolutionLargeur;

                calque[i][j].affiche = false;
                calque[i][j].sprite.setTexture(m_refText);
                calque[i][j].sprite.setScale(m_scaleX, m_scaleY);
                calque[i][j].sprite.setTextureRect(sf::IntRect(x,  y,  m_resolutionLargeur, m_resolutionHauteur)); //On d�coupe la case dans le tileset.bmp
                calque[i][j].sprite.setPosition(j*(m_resolutionLargeur*m_scaleX), i*(m_resolutionHauteur*m_scaleY)); //On le place � la bonne position
            }
        }
    }


    return true;
}

void Mapping::Draw(sf::View &view)
{
    //Affichage de chaque tile de chaque Calque
    for(unsigned int k = 0 ; k < multiCalques.size() ; k++)
        for(unsigned int i = 0 ; i < multiCalques[k].size() ; i++)
            for(unsigned int j = 0 ; j < multiCalques[k][i].size() ; j++)
            {
                this->LiveStream(view, k, i, j);
                if(multiCalques[k][i][j].affiche)
                    fen->draw(multiCalques[k][i][j].sprite);
            }


}

void Mapping::LiveStream(sf::View &view,  unsigned int k, unsigned int i, unsigned int j)
{
    if((j+1)*m_resolutionLargeur*m_scaleX < view.getCenter().x - view.getSize().x/3 && (j+1)*m_resolutionLargeur*m_scaleX > view.getCenter().x - view.getSize().x/2)
        if((int)(rand()%5) == 2)
            multiCalques[k][i][j].opacity -= 50;
    if((j+1)*m_resolutionLargeur*m_scaleX < view.getCenter().x - view.getSize().x/2)
    {
        multiCalques[k][i][j].affiche = false;
        multiCalques[k][i][j].opacity = 0;
    }
    if((j+1)*m_resolutionLargeur*m_scaleX > view.getCenter().x + view.getSize().x/10 && (j+1)*m_resolutionLargeur*m_scaleX < view.getCenter().x + view.getSize().x/2)
    {
        multiCalques[k][i][j].affiche = true;
        if((int)(rand()%5) == 2)
            multiCalques[k][i][j].opacity += 50;
    }
    if(j*m_resolutionLargeur*m_scaleX > view.getCenter().x + view.getSize().x/2)
    {
        multiCalques[k][i][j].affiche = false;
        multiCalques[k][i][j].opacity = 0;
    }
    if((j+1)*m_resolutionLargeur*m_scaleX > view.getCenter().x - view.getSize().x/3 && j*m_resolutionLargeur*m_scaleX < view.getCenter().x + view.getSize().x/10)
    {
        multiCalques[k][i][j].affiche = true;
        multiCalques[k][i][j].opacity = 255;
    }
    multiCalques[k][i][j].opacity = (multiCalques[k][i][j].opacity > 255) ? 255 : multiCalques[k][i][j].opacity;
    multiCalques[k][i][j].opacity = (multiCalques[k][i][j].opacity < 50) ? 50 : multiCalques[k][i][j].opacity;

    multiCalques[k][i][j].sprite.setColor(sf::Color(255,255,255,multiCalques[k][i][j].opacity));
}

bool Mapping::TestCollision(sf::Sprite *sprite, sf::Vector2f position, Direction direction)
{
    if(position.x < 0 || position.y < 0 || position.x >= m_nbBlocsLargeur*m_resolutionLargeur*m_scaleX || position.y >= m_nbBlocsHauteur*m_resolutionHauteur*m_scaleY)
        return false;

    unsigned int x;
    unsigned int y;

    if(direction == BAS)
    {
        x = ((position.x+(sprite->getGlobalBounds().width/2)) / (m_resolutionLargeur*m_scaleX));
        y = ((position.y+sprite->getGlobalBounds().height) / (m_resolutionHauteur*m_scaleY));
        if(y >= multiCalques[0].size() || x >= multiCalques[0][y].size())
            return false;
        for(unsigned int i = 0 ; i < m_vide.size() ; i++)
        {
            if(multiCalques[0][y][x].bloc == m_vide[i])
                return false;
        }
    }
    else if(direction == HAUT)
    {
        x = ((position.x+(sprite->getGlobalBounds().width/2)) / (m_resolutionLargeur*m_scaleX));
        y = (position.y / (m_resolutionHauteur*m_scaleY));
        if(y >= multiCalques[0].size() || x >= multiCalques[0][y].size())
            return false;
        for(unsigned int i = 0 ; i < m_vide.size() ; i++)
        {
            if(multiCalques[0][y][x].bloc == m_vide[i])
                return false;
        }
    }
    else if(direction == DROITE)
    {
        x = ((position.x+sprite->getGlobalBounds().width) / (m_resolutionLargeur*m_scaleX));
        y = ((position.y+sprite->getGlobalBounds().height/2) / (m_resolutionHauteur*m_scaleY));
        if(y >= multiCalques[0].size() || x >= multiCalques[0][y].size())
            return false;
        for(unsigned int i = 0 ; i < m_vide.size() ; i++)
        {
            if(multiCalques[0][y][x].bloc == m_vide[i])
                return false;
        }
    }
    else if(direction == GAUCHE)
    {
        x = (position.x / (m_resolutionLargeur*m_scaleX));
        y = ((position.y+sprite->getGlobalBounds().height/2) / (m_resolutionHauteur*m_scaleY));
        if(y >= multiCalques[0].size() || x >= multiCalques[0][y].size())
            return false;
        for(unsigned int i = 0 ; i < m_vide.size() ; i++)
        {
            if(multiCalques[0][y][x].bloc == m_vide[i])
                return false;
        }
    }

    return true;
}

sf::Vector2f Mapping::getSpawn()
{
    return spawn;
}

int Mapping::getRosolutionLargeur()
{
    return m_resolutionLargeur;
}
int Mapping::getResolutionHauteur()
{
    return m_resolutionHauteur;
}

bool Mapping::Sortie(sf::Vector2f pos)
{
    if(pos.y >= m_nbBlocsHauteur * m_resolutionHauteur * m_scaleY)
        return true;
    return false;
}

int Mapping::getLongeurStage()
{
    return m_resolutionLargeur*m_nbBlocsLargeur*m_scaleX;
}
int Mapping::getHauteurStage()
{
    return m_resolutionHauteur*m_nbBlocsHauteur*m_scaleY;
}

std::string Mapping::getNom()
{
    return nomMap;
}

sf::Mutex* Mapping::getMutex()
{
    return &mutexMap;
}
