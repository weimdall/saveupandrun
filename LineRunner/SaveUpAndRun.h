/**
 * \file SaveUpAndRun.h
 * \brief Gestion du jeu
 * \author J.Cassagne
 */

#ifndef SAVEUPANDRUN_H
#define SAVEUPANDRUN_H

#include <sstream>
#include <string>
#include "Animation.h"
#include "Son.h"
#include "Mapping.h"
#include "Serveur.h"
#define VITESSE 9
#define JUMP 17.5

struct CelluleScore
{
    sf::RectangleShape r;
    sf::Text t[2];
};

class SaveUpAndRun
{
    public:
        SaveUpAndRun();
        virtual ~SaveUpAndRun();

    private:
        bool DemandePseudo();
        void Boucle();
        bool Charger();
        void Gravite(float v);
        void Debug();
        void Score();
        void TblScore();
        void Restart();
        void Meurt();
        void Menu();
        void Draw();

        Joueur joueur;
        sf::View view;
        sf::Vector2f sizeView;
        bool sol;
        bool debug;
        bool menu;
        sf::Font police;
        sf::Text texteDebug;
        sf::Text score;
        float vitesse;
        sf::RenderWindow *app;
        sf::Texture fond;
        sf::Sprite sprFond;
        Mapping *stage;
        sf::Text nomMap;
        sf::Clock clock, clock2;

        std::vector<Joueur> tblJoueurs;
        CelluleScore tblScore[10];
        Serveur *serveur;
};

#endif // SAVEUPANDRUN_H
