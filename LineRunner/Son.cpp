/**
 * \file Son.cpp
 * \brief Gestion du Son
 * \author J.Cassagne
 */

#include "Son.h"

using namespace sf;

Son::Son(std::string chemin, bool boucle)
{
    m_son.openFromFile(chemin);
    m_son.setLoop(boucle);
}

void Son::Lire()
{
    m_son.play();
}
void Son::Pause()
{
    m_son.pause();
}
void Son::Stop()
{
    m_son.stop();
}
void Son::setVolume(float volume)
{
    m_son.setVolume(volume);
}
bool Son::enLecture()
{
    if(m_son.getStatus() == Sound::Playing)
        return true;
    else
        return false;

}
void Son::setSon(std::string chemin, bool boucle)
{
    m_son.stop();
    m_son.openFromFile(chemin);
    m_son.setLoop(boucle);
}
