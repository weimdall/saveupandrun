/**
 * \file Mapping.h
 * \brief Gestion du TileMapping
 * \author J.Cassagne
 */

#ifndef MAPPING_H
#define MAPPING_H

#include <vector>
#include <fstream>
#include <SFML/Graphics.hpp>
#include <time.h>

#define MAPS "data/"

struct Tile
{
    int bloc;
    sf::Sprite sprite;
    bool affiche;
    int opacity;
};

enum Direction
{
    HAUT, BAS, DROITE, GAUCHE, ATTEND
};
typedef enum Direction Direction;


class Mapping
{
    public:
        Mapping(sf::RenderWindow *App);
        virtual ~Mapping();
        bool TestCollision(sf::Sprite *sprite, sf::Vector2f position, Direction direction);
        bool LireFichier(std::string nomFichier);
        void Draw(sf::View &view);
        bool Sortie(sf::Vector2f pos);
        sf::Vector2f getSpawn();

        int getRosolutionLargeur();
        int getResolutionHauteur();
        int getLongeurStage();
        int getHauteurStage();
        std::string getNom();
        sf::Mutex* getMutex();

    private:
        bool ChargerMap(std::vector <std::vector<Tile> >&calque);
        void LiveStream(sf::View &view, unsigned int k, unsigned int i, unsigned int j);

        sf::RenderWindow *fen;
        sf::Image m_ref;
        sf::Texture m_refText;
        std::vector <std::vector<std::vector<Tile> > >multiCalques;
        std::vector <int>m_vide;
        sf::Vector2f spawn;
        std::string nomMap;

        int m_nbTileHauteur;
        int m_nbTileLargeur;
        int m_resolutionHauteur;
        int m_resolutionLargeur;
        int m_nbBlocsLargeur;
        int m_nbBlocsHauteur;
        float m_scaleX;
        float m_scaleY;
        sf::Mutex mutexMap;
};

#endif // MAPPING_H
